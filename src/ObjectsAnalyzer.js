'use strict';

const IO = require('./IO');
let io = new IO();

function ObjectsAnalyzer(metadataFilePath, pathsToEvaluate) {
    this.metadataFilePath = metadataFilePath;
    this.pathsToEvaluate = pathsToEvaluate;
}

ObjectsAnalyzer.prototype.getAvailableFields = function (objectType, cb) {
    let availableFields = [];
    io.readXML(this.metadataFilePath, (err, data) => {
        if (!err) {
            let productTypeExtension = data.children.filter(extension => extension.attributes['type-id'] == objectType)[0];
            let customAttributesDefinitions = productTypeExtension.children.filter(typeExtension => typeExtension.name == 'custom-attribute-definitions')[0];
            customAttributesDefinitions.children.forEach(attributeDefinition => {
                let attributeId = attributeDefinition.attributes['attribute-id'];
                availableFields.push(attributeId);
            });

            cb(null, availableFields);
        } else {
            cb(err);
        }
    });
}

ObjectsAnalyzer.prototype.getRemovableFields = function (objectType, cb) {

    let results = {},
        resultsQueue = [];

    function filterNonRemovableFields() {
        let fieldsToRemove = [];
        for (const attribute in results) {
            const element = results[attribute];
            let count = 0;
            let exactMatches = element.filter(e => '.' + e.attribute == e.match);
            if (exactMatches && exactMatches.length > 0) {
                count = exactMatches.map(e => e.count)
                    .reduce((acc, cur) => acc + cur);
            } else {
                let possibleJsMatches = element
                    .filter(e => ('"' + e.attribute == e.match || '\'' + e.attribute == e.match) &&
                        (e.file.endsWith('.js') || e.file.endsWith('.isml')));
                if (possibleJsMatches.length > 0) {
                    count = possibleJsMatches.map(e => e.count)
                        .reduce((acc, cur) => acc + cur);
                }
            }

            if (count == 0) {
                let nonEmptyMatches = element.filter(e => e.count > 0);
                let checkManually = false;

                if (!(nonEmptyMatches.length == 1 &&
                        nonEmptyMatches[0].file.indexOf('system-objecttype-extensions') > -1)) {
                    checkManually = true;
                }

                fieldsToRemove.push({
                    data: element,
                    matches: nonEmptyMatches,
                    checkManually: checkManually
                });
            }
        }

        return fieldsToRemove;
    }

    function groupResults() {
        resultsQueue.forEach(result => {
            if (!(result.attribute in results)) {
                results[result.attribute] = [result];
            } else {
                results[result.attribute].push(result);
            }
        });
        return filterNonRemovableFields();
    }

    function addSearchResult(result) {
        resultsQueue.push(result);
    }

    this.getAvailableFields(objectType, (err, availableFields) => {
        let self = this;
        if (!err) {
            console.log(`${availableFields.length} fields found...`);
            console.log('Searching in source files...');

            let findInFiles = require('find-in-files');
            let elementsProcessedCount = 0;

            availableFields.forEach(attributeId => {
                this.pathsToEvaluate.forEach(element => {
                    findInFiles.find({
                            'term': '.' + attributeId,
                            'flags': 'g'
                        }, element.path, element.extension)
                        .then(function (searchResult) {
                            let propertyCount = 0;
                            for (var result in searchResult) {
                                var res = searchResult[result];
                                addSearchResult({
                                    attribute: attributeId,
                                    count: res.count,
                                    file: result,
                                    match: res.matches[0]
                                });
                                propertyCount++;
                            }
                            if (propertyCount == 0) {
                                addSearchResult({
                                    attribute: attributeId,
                                    count: 0
                                });
                            }

                            elementsProcessedCount++;

                            if (elementsProcessedCount == self.pathsToEvaluate.length * availableFields.length) {
                                console.log('Search finished...');
                                cb(groupResults());
                            }
                        });
                });
            });
        }
    });
}

module.exports = ObjectsAnalyzer;