const fs = require('fs');

function IO() {

}

IO.prototype.readXML = function(path, cb) {

    fs.readFile(path, 'utf8', (err, xml) => {
        if (!err) {
            const XmlReader = require('xml-reader');
            const reader = XmlReader.create();

            reader.on('done', data => {
                cb(null, data);
            });

            reader.parse(xml);
        } else {
            cb(err);
            console.log('error' + err);
        }
    });
}

IO.prototype.saveFile = function(path, content) {
    fs.writeFile(path, content);
}

module.exports = IO;