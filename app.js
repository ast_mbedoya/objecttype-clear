let ObjectsAnalyzer = require('./src/ObjectsAnalyzer');
const IO = require('./src/IO');

const resultsFile = './fields-to-remove-results.txt',
    metadataFilePath = '../charly/site_template/meta/system-objecttype-extensions.xml',
    extensionsToCheck = '(.isml|.js|.xml)$',
    pathsToEvaluate = [{
        path: '../charly/app_storefront_core_ext/cartridge/templates/default',
        extension: extensionsToCheck
    }, {
        path: '../charly/app_storefront_core_ext/cartridge/scripts',
        extension: extensionsToCheck
    },{
        path: '../charly/app_storefront_core/cartridge/templates/default',
        extension: extensionsToCheck
    }, {
        path: '../charly/app_storefront_core/cartridge/scripts',
        extension: extensionsToCheck
    }, {
        path: '../charly/app_storefront_controllers/cartridge/scripts',
        extension: extensionsToCheck
    }, {
        path: '../charly/int_powerreviews',
        extension: extensionsToCheck
    }, {
        path: '../charly/site_template',
        extension: extensionsToCheck
    }];


function saveResults(path, content) {
    let io = new IO();
    io.saveFile(resultsFile, stringResults);
    console.log('Results saved to ' + resultsFile);
}

let objectTypesToAnalize = ['Store', 'Product', 'Category'];
objectsAnalyzer = new ObjectsAnalyzer(metadataFilePath, pathsToEvaluate);

console.log('Starting Analysis of fields...');

let stringResults = '',
    objectsAnalyzedCount = 0;
objectTypesToAnalize.forEach(objectType => {
    console.log(`Analysis of  ${objectType} started...`);
    objectsAnalyzer.getRemovableFields(objectType, fieldsToRemove => {

        console.log(`${fieldsToRemove.length} fields to be removed \r\n`);
        stringResults += 'Analisys of ' + objectType + '\r\n';
        fieldsToRemove.forEach(element => {
            let removalMessage = element.checkManually ? 'MIGHT BE' : 'CAN BE';
            stringResults += `${element.data[0].attribute} ${removalMessage} REMOVED \r\n`;
            stringResults += JSON.stringify(element.matches) + '\r\n';
            stringResults += '------------------------------->\r\n';
        });
        console.log(`Analysis of  ${objectType} completed...`);

        objectsAnalyzedCount++;

        if (objectsAnalyzedCount == objectTypesToAnalize.length) {
            saveResults(resultsFile, stringResults);
        }
    });
});